**About**
This repository contains HTML5 and CSS3 code for a college project about the author Haruki Murakami. It contains examples of HTML and CSS formatting
for tables, forms, and navigation bars.

**How to use this repository**
To use this project, simply clone the repository following Atlassian guidelines (found here: https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html). 

To run the index.html on a browser, right click on the index.html and chose Open With and click the browser of your choice. 

To edit or view the contents of any file, right click and open in a text editor - I recommend Notepad ++. 

**License**
This project is licensed under the MIT Open Source License. The MIT License is the most popular open source licenses. It allows developpers to use software under
the condition that the original developper is acknowledged (the original copyright must be included), the project remains open source, and that the user
understands that the original developper holds no liability for any pitfalls of the code.
